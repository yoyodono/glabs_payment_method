Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :bank_details
  end

  get '/bank_details/:id' => 'bank_details#bank_details'

end
