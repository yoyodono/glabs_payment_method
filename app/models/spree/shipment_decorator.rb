Spree::Shipment.class_eval do

  state_machine :initial => 'pending' do
    event :ship do
      transition :from => 'pending', :to => 'shipped', :if => :check_cod_if_true?
    end
  end

  def check_cod_if_true?
    return_value = false
    self.order.payments.each do |payment|
      if payment.payment_method.type == "Spree::PaymentMethod::CashOnDelivery" && payment.state != 'void' && self.order.shipment_state != "backorder"
        return_value = true
        break
      end
    end
    return_value
  end

end
