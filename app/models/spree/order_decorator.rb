Spree::Order.class_eval do

  def bank_detail
    if self.payment_method.type == "Spree::PaymentMethod::BankTransfer"
      Spree::BankDetail.find(self.payment.source_id)
    end
  end

end
