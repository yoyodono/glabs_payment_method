class Spree::BankDetail < ActiveRecord::Base
  attr_accessible :account_name, :account_number, :bank, :bank_branch, :status
  has_many :payments, :as => :source, :class_name => 'Spree::Payment'

  validates :account_name, :account_number, :bank, :presence => true

end
