Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "glabs_payment_method_admin_shared_tabs",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :text => "<%= tab(:bank_details, :url => admin_bank_details_path) %>")
