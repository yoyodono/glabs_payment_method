Spree::CheckoutController.class_eval do

  after_filter :bank_detail, :only => [:update]
  before_filter :bank_validation, :only => [:update]

  # To add bank detail for detail information of customer's bank that used for their payment
  def bank_detail
    if current_order.payment.present? && current_order.payment.source_id.nil?
      if current_order.payment.payment_method.type == "Spree::PaymentMethod::BankTransfer"
        @bank_detail = Spree::BankDetail.new(params[:bank])
        if @bank_detail.valid?
          @bank_detail.save
          current_order.payment.update_attributes(:source_id => @bank_detail.id)
        end
      end
    end
  end

  # To validate the bank's informations while checking out the orders on the front end
  def bank_validation
    if params[:bank]
      if Spree::PaymentMethod.find(params[:order][:payments_attributes][0][:payment_method_id]).type == "Spree::PaymentMethod::BankTransfer"
        @bank_detail = Spree::BankDetail.new(params[:bank])
        if @bank_detail.valid?
        else
          flash[:error] = "Bank informations was incomplete"
          redirect_to :back
          return false
        end
      end
    end
  end

end
