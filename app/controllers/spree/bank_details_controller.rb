class Spree::BankDetailsController < ApplicationController

  def bank_details
    @bank_owner = Spree::BankDetail.find_by_bank(params[:id], :conditions => {:status => "owner"})
    respond_to do |format|
      format.json {render :json => @bank_owner}
    end
  end

end
