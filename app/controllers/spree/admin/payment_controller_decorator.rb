Spree::Admin::PaymentsController.class_eval do
  after_filter :bank_detail, :only => [:create]

  def bank_detail
    if @payment.payment_method.type == "Spree::PaymentMethod::BankTransfer"
      @bank_detail = Spree::BankDetail.create(params[:bank])
      @payment.update_attributes(:source_id => @bank_detail.id)
    end
  end

end
