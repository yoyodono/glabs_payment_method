Spree::Admin::PaymentsController.class_eval do

  after_filter :bank_detail, :only => [:create]
  before_filter :bank_validation, :only => [:create]

  # To add bank detail for detail information of customer's bank that used for their payment
  def bank_detail
    if @order.payments.last.payment_method.type == "Spree::PaymentMethod::BankTransfer"
      @bank_detail = Spree::BankDetail.new(params[:bank])
      if @bank_detail.valid?
        @bank_detail.save
        @order.payments.last.update_attributes(:source_id => @bank_detail.id)
      end
    end
  end

  # To validate the bank's informations while making payment
  def bank_validation
    if params[:bank]
      if Spree::PaymentMethod.find(params[:payment][:payment_method_id]).type == "Spree::PaymentMethod::BankTransfer"
        @bank_detail = Spree::BankDetail.new(params[:bank])
        if @bank_detail.valid?
        else
          flash[:error] = "Bank informations was incomplete"
          redirect_to :back
          return false
        end
      end
    end
  end

end
