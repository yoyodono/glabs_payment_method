module Spree
  module Admin
    class BankDetailsController < ResourceController

      def index
        @bank_details = BankDetail.all(:conditions => {:status => "owner"})
      end

      def new
        @bank_detail = BankDetail.new
      end

      def edit
        @bank_detail = BankDetail.find(params[:id])
      end

      def create
        @bank_detail = BankDetail.new(params[:bank_detail])
        if @bank_detail.save
          flash.notice = I18n.t(:successfully_created, :resource => I18n.t(:bank_detail))
          respond_with(@bank_detail, :location => admin_bank_details_path(@bank_detail))
        else
          #invoke_callbacks(:create, :fails)
          respond_with(@bank_detail)
        end
      end

    end
  end
end

