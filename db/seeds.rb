p = Spree::PaymentMethod.new
p.type = "Spree::PaymentMethod::BankTransfer"
p.name = "Bank Transfer Payment"
p.description = "Payment by Transferring to specified Bank"
p.environment = "development"
p.save
