class CreateSpreeBankDetails < ActiveRecord::Migration
  def change
    create_table :spree_bank_details do |t|
      t.string :bank
      t.string :account_name
      t.string :account_number
      t.string :bank_branch
      t.string :account_status #owner or customer
      t.string :status
      t.timestamps
    end
  end
end
